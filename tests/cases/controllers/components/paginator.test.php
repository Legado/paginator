<?php
/**
 * PaginatorComponentTest file
 *
 * Long description for file
 *
 * PHP versions 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @package       paginator
 * @subpackage    mtc.paginator.tests.cases.controller.components
 * @author        marcola (Marcus Lacerda) / MicroTC
 * @since         Paginator Plugin v 0.9.1
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 *
 */
App::import('Component','Paginator.Paginator');
App::import('Test','Paginator.User','Paginator.Tag');
/**
 * PaginatorTestController class
 *
 * @package       paginator
 * @subpackage    mtc.paginator.tests.cases.controller.components
 */
class PaginatorTestController extends Controller {

}
/**
 * PaginatorComponentTest class
 *
 * @package       paginator
 * @subpackage    mtc.paginator.tests.cases.controller.components
 *
 */
class PaginatorComponentTest extends CakeTestCase {

    var $fixtures = array('plugin.paginator.user','plugin.paginator.tag');
    
    /**
     * setUp method
     *
     * @access public
     * @return void
     */
    function startCase() {
        $this->PaginatorComponentTest =& new PaginatorComponent();
        $controller =& new PaginatorTestController();
        $controller->params['action'] = 'index';
        $controller->UserTest =& new User();
        $this->PaginatorComponentTest->startup(&$controller);      
    }

    public function testInit(){
        //$controller->UserTest =& new User();
        //debug($controller->UserTest->find('all'));
    }
}