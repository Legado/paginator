<?php


/**
 * Tag class
 *
 * @package       cake
 * @subpackage    cake.tests.cases.libs.model
 */
class Tag extends CakeTestModel {
    /**
     * name property
     *
     * @var string 'Tag'
     * @access public
     */
    var $name = 'Tag';
}

?>
