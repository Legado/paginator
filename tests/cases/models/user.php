<?php
class User extends CakeTestModel {
    /**
     * name property
     *
     * @var string 'User'
     * @access public
     */
    var $name = 'User';
    /**
     * validate property
     *
     * @var array
     * @access public
     */
    var $validate = array('user' => 'notEmpty', 'password' => 'notEmpty');
}
?>