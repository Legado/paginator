<?php
/**
 * ArticlesTag class
 *
 * @package       cake
 * @subpackage    cake.tests.cases.libs.model
 */
class ArticlesTag extends CakeTestModel {
    /**
     * name property
     *
     * @var string 'ArticlesTag'
     * @access public
     */
    var $name = 'ArticlesTag';
}
?>
