<?php
/**
 * Helper Ext Paginator class.
 *
 * PHP versions 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @package       paginator
 * @subpackage    mtc.paginator.helpers
 * @author        marcola (Marcus Lacerda) / MicroTC
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<?php
/**
 * Include files
 */
if (!class_exists('PaginatorHelper')) {
    App::import('Helper', 'Paginator');
}
/**
 * ExtPaginatorHelper
 *
 * Classe responsável por criação de componentes de tela relacionada a paginação.
 *
 * @todo realizar testes.
 * @package       paginator
 * @subpackage    mtc.paginator.helpers
 * @author        marcola (Marcus Lacerda)
 * @todo revisar comentários.
 */
class ExtPaginatorHelper extends PaginatorHelper {

    /**
     * Como o método do PaginatorHelper porém acrescenta na url os campos do filtro.
     *
     * @param array $options
     * @param bool $asArray
     * @param string $model Which model to paginate on
     * @return mixed Por padrão, retorna uma string com a URL.
     */
    public function url($options = array(), $asArray = false, $model = null) {
        if(isset($this->params['search'])) {
            $options = array_merge($this->params['search'],$options);
        }
        return parent::url($options,$asArray,$model);
    }

    /**
     * Como a função pai porém pode-se retornar um único parametro que possui a chave $key,
     * caso nao exista valor para a chave, será retornado o valor $default.
     *
     * @param string $model nome do modelo. usa o padrão se nenhum for passado.
     * @param string $key indice para um valor de parametro
     * @param mixed $default
     * @return mixed paramatros ou parametro da paginação
     */
    public function params($model = null,$key = null,$default = null) {
        $params = parent::params($model);
        if($key === null) {
            return $params;
        }
        return isset($params[$key])? $params[$key] : $default ;
    }

    /**
     * Retorna a primeira página ou as $first primeiras páginas.
     *
     * @param string $first texto que será exibido quando o link estiver ativo.
     * @param array $options options html para $title e options para a paginacao
     * @param mixed $disabledTitle string que será mostrada se o link estiver desabilitado. por padrao será exibido $title.
     * @param array $disabledOptions html options para o disabledTitle
     * @return string Retorna o link 'first' ou o disabledTitle se ja estiver na primeira página.
     */
    public function first($first = '<< first', $options = array(), $disabledTitle = true, $disabledOptions = array() ) {
        $_defaults = array(
                'model' => null,
        );
        $options = array_merge($_defaults,$options);

        if(is_string($first)) {
            if(empty ($disabledOptions)) {
                $disabledOptions = $options;
            }
            return $this->__move('First',$first,$options,$disabledTitle,$disabledOptions);
        }
        elseif(is_int($first) && $this->params($options['model'],'page') > $first) {
            return $this->firsts(intval($first), $options);
        }
    }

    /**
     * Retorna as $count primeiras páginas.
     *
     * @param int $count quantidade de paginas.
     * @param array $options parametros opcionais.
     * @return string retorna os links html para as paginas.
     */
    public function firsts($count, $options = array()) {
        $end = $count;
        $start = 1;

        if(!isset($options['after']) || $options['after'] === null) {
            $options['after'] = '...';
        }

        return $this->__numbers($start, $end, $options);
    }

    /**
     * Retorna a ultima página ou as $last últimas páginas.
     *
     * @param string $last texto que será exibido quando o link estiver ativo.
     * @param array $options options html para $title e options para a paginacao
     * @param mixed $disabledTitle string que será mostrada se o link estiver desabilitado. por padrao será exibido $title.
     * @param array $disabledOptions html options para o disabledTitle
     * @return string Retorna o link 'last' ou o $disabledTitle se já estiver na ultima página.
     */
    public function last($last = 'last >>', $options = array(), $disabledTitle = true, $disabledOptions = array() ) {
        $_defaults = array(
                'model' => null,
        );
        $options = array_merge($_defaults,$options);

        $lower = $this->params($options['model'],'pageCount') - $last + 1;
        $page = $this->params($options['model'],'page',1);

        if(is_string($last)) {
            return $this->__move('Last',$last,$options,$disabledTitle,$disabledOptions);
        }
        elseif(is_int($last) && $page < $lower ) {
            return $this->lasts(intval($last), $options);
        }
    }

    /**
     * Retorna as $count páginas últimas paginas
     *
     * @param int $count quantidade de paginas.
     * @param array $options parametros opcionais.
     * @return string retorna os links html para as paginas.
     */
    public function lasts($count, $options = array()) {
        $end = $this->params($options['model'],'pageCount');
        $start = $end - $count + 1;

        if(!isset($options['before']) || $options['before'] === null) {
            $options['before'] = '...';
        }

        return $this->__numbers($start, $end, $options);
    }

    /**
     * Retorna a próxima página
     *
     * @param string $title texto que será exibido quando o link estiver ativo.
     * @param array $options options html para $title e options para a paginacao
     * @param mixed $disabledTitle string que será mostrada se o link estiver desabilitado. por padrao será exibido $title.
     * @param array $disabledOptions html options para o disabledTitle
     * @return string retorna o link 'next' ou o $disabledTitle se já estiver na ultima página.
     */
    public function next( $title = 'next >>', $options = array(), $disabledTitle = true, $disabledOptions = array() ) {
        return $this->__move('Next',$title,$options,$disabledTitle,$disabledOptions);
    }

    /**
     * Retorna a página anterior
     *
     * @param string $title texto que será exibido quando o link estiver ativo.
     * @param array $options options html para $title e options para a paginacao
     * @param mixed $disabledTitle string que será mostrada se o link estiver desabilitado. por padrao será exibido $title.
     * @param array $disabledOptions html options para o disabledTitle
     * @return string Retorna o link 'prev' ou o disabledTitle se ja estiver na primeira página.
     */
    public function prev( $title = '<< prev', $options = array(), $disabledTitle = true, $disabledOptions = array() ) {
        return $this->__move('Prev',$title,$options,$disabledTitle,$disabledOptions);
    }

    /**
     * Verifica se haverá link para a primeira página.
     *
     * @param string $model
     * @return boolean retorna <i>false</i> caso já esteja na primeira pagina e <i>true</i> caso contrário
     */
    public function hasFirst($model = null) {
        $page = $this->params($model, 'page', 1);
        return $page > 1;
    }

    /**
     * Verifica se haverá link para a última página.
     *
     * @param string $model
     * @return boolean retorna <i>false</i> caso já esteja na última página e <i>true</i> caso contrário.
     */
    public function hasLast($model = null) {
        $pageCount = $this->params($model, 'pageCount');
        $page = $this->params($model, 'page', $pageCount);
        return $page < $pageCount;
    }

    /**
     * @todo refatorar este metodo
     * @todo realizar testes, analizar quando nao houver dados e fazer adequadamente os comentarios
     * @param string $title
     * @param string $key
     * @param array $options
     * @return string
     */
    public function sort($title,$key=null,$options=array()) {

        $defaultOptions = array(
                'escape' => false,
                'imgAsc' =>  null,
                'imgDesc' => null,
                'url' => array(),
                'model' => null,
        );

        $options = array_merge($defaultOptions,(array)$options);
        $url = $options['url'];
        unset($options['url']);

        if (empty($key)) {
            $key = $title;
            $title = __(Inflector::humanize(preg_replace('/_id$/', '', $title)), true);
        }

        //include image
        $image = '';
        if(!empty($options['imgDesc']) || !empty($options['imgAsc']) ) { //se foram passados as imagens
            $image = ' ' . $this->sortImage($options['model'], $key, $options);
        }

        unset($options['imgDesc']);
        unset($options['imgAsc']);

        $dir = 'asc';
        $sortKey = $this->sortKey($options['model']);
        $sortDir = $this->sortDir($options['model']);
        $sortCount = $this->params($options['model'],'sortCount');

        if( is_int($sortCount) ) { //include multisort
            $isSorted = in_array($key, $sortKey);

            if(!$isSorted) {
                $index = $sortCount + 1;
            } else {
                $index = array_keys($sortKey, $key);
                $index = $index[0];

                $dir = ($sortDir[$index] === 'asc') ? 'desc' : 'asc';
            }

            $change_url = array('sort'.$index => $key, 'direction'.$index => $dir);

        }
        else { //if is not multisort

            $isSorted = ($sortKey === $key || $sortKey === $this->defaultModel() . '.' . $key);

            if ($isSorted && $sortDir === 'asc') {
                $dir = 'desc';
            }

            $change_url = array('sort' => $key, 'direction' => $dir);
        }

        if (is_array($title) && array_key_exists($dir, $title)) {
            $title = $title[$dir];
        }

        $url = array_merge($change_url,$url,array('order' => null));

        $content = trim($title . $image);
        return $this->link( $content, $url, $options);
    }


    /**
     * Identifica se a string $caption é uma url para imagem.
     * Caso positivo constrói e retorna o html referente a imagem.
     * Caso contrário retorna o $caption.
     *
     * @param string $title
     * @param array $options atributos html para a imagem
     * @return string retorna a imagem caso $caption se refira a uma imagem ou $caption sem alterações.
     */
    private function __checkImage($caption,$options=array()) {
        if (strpos($caption, '://') !== false
                || preg_match('/\.(jpg|jpe|jpeg|gif|png|ico)$/', $caption) ) {
            return true;
        }
        return false;
    }

    /**
     *
     * @todo fazer adequadamente os comentarios e refatorá-la
     * @param <type> $key
     * @param <type> $model
     * @param <type> $options
     * @return <type>
     */
    private function sortImage($model=null, $key=null, $options=array()) {
        $urlImage = null;

        $sortDir = $this->sortDir($model);
        $sortKey = $this->sortKey($model);

        if ( ! is_int ($this->params($options['model'],'sortCount')) ) {
            if( $sortKey == $key || $sortKey == $this->defaultModel() . '.' . $key ) {
                if( $sortDir  == 'desc') {
                    $urlImage = $options['imgDesc'];
                }
                elseif( $sortDir == 'asc') {
                    $urlImage = $options['imgAsc'];
                }
            }
        }
        else {
            $indexes = array_keys($sortKey,$key);
            if(!$indexes) {
                $indexes = array_keys($sortKey,$this->defaultModel() . '.' . $key);
            }

            if( isset($indexes[0]) ) {
                $index = $indexes[0];
                if( $sortDir[$index]  == 'desc') {
                    $urlImage = $options['imgDesc'];
                }
                elseif( $sortDir[$index] == 'asc') {
                    $urlImage = $options['imgAsc'];
                }
            }
        }

        if($urlImage)
            return $this->Html->image($urlImage);
        return null;
    }


    /**
     *
     * @param <type> $model
     * @param <type> $options
     * @return <type>
     */
    function sortKey($model = null, $options = array()) {
        if( !is_int($this->params($model,'sortCount')) ) {
            return parent::sortKey($model, $options);
        }
        else { //multisort
            if (empty($options)) {
                $params = $this->params($model);
                $options = array_merge($params['defaults'], $params['options']);
            }

            $sortKeys = array();
            for($i = 1; $i <= $params['sortCount']; $i++) {
                $sortKeys[$i] = $options['sort'.$i];
            }
            return $sortKeys;
        }
    }

    /**
     *
     * @param <type> $model
     * @param <type> $options
     * @return <type>
     */
    function sortDir($model = null, $options = array()) {
        if( !is_int($this->params($model,'sortCount')) ) {
            return parent::sortDir($model, $options);
        }
        else { //multisort
            if (empty($options)) {
                $params = $this->params($model);
                $options = array_merge($params['defaults'], $params['options']);
            }

            $sortDirs = array();
            for($i = 1; $i <= $params['sortCount']; $i++) {
                /*if (isset($options['direction'.$i])) {
			$sortDirs[$i] = strtolower($options['direction'.$i]);
		} elseif (isset($options['order']) && is_array($options['order']))
			$sortDirs[$i] = strtolower(current($options['order']));
		}*/
                $sortDirs[$i] = isset($options['direction'.$i]) ? strtolower($options['direction'.$i]) : '';
                if($sortDirs[$i] != 'desc') {
                    $sortDirs[$i] = 'asc';
                }
            }
            return $sortDirs;
        }
    }


    /**
     * Função generica que retorna uma pagina para 'First','Last','Next' ou 'Prev'
     *
     * @param string $title texto que será exibido quando o link estiver ativo.
     * @param array $options options html para $title e options para a paginacao
     * @param mixed $disabledTitle string que será mostrada se o link estiver desabilitado. por padrao será exibido $title.
     * @param array $disabledOptions html options para o disabledTitle
     * @return string retorna o link para uma pagina ou o disabledTitle se não houver a página requisitada.
     */
    protected function __move( $which, $title, $options, $disabledTitle, $disabledOptions) {
        $_defaults = array(
                'tag' => 'div',
                'class' => 'enable',
                'before' => '',
                'after' => '',
                'escape' => true,
                'separator' => '',
                'url' => '');

        if($disabledTitle === null) {
            $disabledTitle = true;
        }

        if( $this->__checkImage($title) ) {
            $title = $this->Html->image($title);
            $content = $this->__pagingLink($which, $title, array_merge((array)$options,array('escape' => false)));
        }
        else {
            $content = $this->__pagingLink($which, $title, (array)$options);
        }

        if(empty($content) && $disabledTitle) {
            if( $disabledTitle === true ) {
                $content = $title;
            }
            else {
                if( $this->__checkImage($disabledTitle) ) {
                    $disabledTitle = $this->Html->image($disabledTitle);
                    $disabledOptions['escape'] = false;
                }
                $content = $disabledTitle;
            }

            $disabledDefaultOptions = array(
                    'class' => 'disabled',
            );
            $options = array_merge($_defaults,$disabledDefaultOptions,(array)$disabledOptions);

            if($options['escape']) {
                $content = h($content);
            }
        }
        else {
            $options = array_merge($_defaults,(array)$options);
        }

        unset($options['model']);
        unset($_defaults['class']);

        foreach ( array_keys($_defaults) as $key) {
            ${$key} = $options[$key];
            unset($options[$key]);
        }

        return $before . $this->Html->tag($tag, $content, $options) . $after;
    }

    /**
     * Retorna as paginas compreendidas no intervalo [$start,$end].
     *
     * @param int $start primeira pagina do intervalo.
     * @param int $end ultima pagina do intervalo.
     * @param array $options parametros opcionais.
     * @return string retorna os links html para as paginas.
     */
    protected function __numbers($start, $end, $options = array()) {
        extract($options);

        $_defaults = array(
                'tag' => 'span',
                'before' => '',
                'after' => '',
                'model' => null,
                'separator' => ' | ',
        );

        $options = array_merge($_defaults,$options);
        foreach( array_keys($_defaults) as $key) {
            ${$key} = $options[$key];
            unset($options[$key]);
        }

        $out = ''. $before ;
        for($i = $start; $i <= $end; $i++) {
            $out .= $this->Html->tag($tag,$this->link($i,array('page'=>$i)),$options);
            if($i != $end) {
                $out .= $separator;
            }
        }
        $out .= $after;

        return $out;
    }

    /**
     * Obtém o número da página requisitada.
     *
     * @param string $model
     * @param string $page ('First', 'Last', 'Prev' ou 'Next')
     * @return mixed retorna o número da pagina requisitada ou false caso a pagina não seja possível
     */
    protected function __page($model, $page) {
        $check = 'has'.$page;

        if( !$this->{$check}($model) ) {
            return false;
        }
        switch ($page) {
            case 'Next':
                $current = $this->current($model);
                return ++$current;
            case 'Prev':
                $current = $this->current($model);
                return --$current;
            case 'First':
                return 1;
            case 'Last':
                $paging = $this->params($model);
                return $paging['pageCount'];
        }
    }

    /**
     * Constrói o link para paginação
     *
     * @access protected
     * @param string $which que tipo de link será ('First' 'Last' 'Prev' 'Next')
     * @param string $title conteúdo do link
     * @param array $options options para a paginação
     * @return string retorna o link ou null caso o link caso nao seja possivel.
     */
    function __pagingLink($which, $title = null, $options = array()) {
        $_defaults = array(
                'url' => array(),
                'model' => null,
                'escape' => true);


        unset($options['separator'],$options['before'],$options['after'],$options['tag']);
        $options = array_merge($_defaults, (array)$options);

        foreach ( array_keys($_defaults) as $key) {
            ${$key} = $options[$key];
            unset($options[$key]);
        }

        $page = $this->__page($model, $which);
        if ($page === false) {
            return null;
        }

        $url = array_merge(array('page' => $page), $url);
        return $this->link($title, $url, array_merge($options, array('escape' => $escape)));
    }
}
?>
