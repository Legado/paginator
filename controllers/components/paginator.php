<?php
/**
 * Component Paginator class.
 *
 * PHP versions 5
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @package       paginator
 * @subpackage    mtc.paginator.component
 * @author        marcola (Marcus Lacerda) / MicroTC
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<?php
/**
 * Include files
 */
/**
 * PaginatorComponent
 *
 * Classe responsável por centralizar as funções relativas ao controle de paginação.
 *
 * @package       paginator
 * @subpackage    mtc.paginator.component
 * @author        marcola (Marcus Lacerda)
 *
 */
class PaginatorComponent extends Object {

    /**
     * Components utilizados
     * @var array components
     */
    public $components = array('RequestHandler');

    /**
     * Controller que utilizará a paginação.
     * @var Controller controller
     */
    private $controller = null;

    /**
     * Define se será utilizado multi sort na paginação. (Mantendo multiplas colunas ordenadas).
     * @var bool multisort
     */
    private $multisort = false;

    //mensagens

    /**
     * Mensagem que indica a falta do atributo $extPaginator no controller.
     */
    const NO_EXT_PAGINATE_ERROR = 'can\'t find the attribute extPaginate in controller %1$sController';

    /**
     * Mensagem que indica quando se passa um tipo de filtro desconhecido.
     */
    const NO_TYPE_FILTER_ERROR = 'can\'t find type %1$s';

    /**
     * Obtem referência para atributos do @param $controller serão lidas e/ou modificadas.
     * @param Controller $controller
     * @param array $settings
     * @access public
     */
    public function initialize(&$controller, $settings = array()) {
        $this->controller = $controller;
        if(!isset($controller->extPaginate)) {
            $this->error("initialize",self::NO_EXT_PAGINATE_ERROR, $controller->name);
            return;
        }
        if(isset($settings['multisort'])) {
            $this->multisort = $settings['multisort'];
        }

        if($this->multisort) {
            if(isset($controller->params['sortCount'])) {
                $count = $controller->params['sortCount'];
                $options = array_merge($controller->params, $controller->params['url'], $controller->passedArgs);
                if(isset($options['sort'.($count+1)])) {
                    $controller->params['sortCount']++;
                }
            }
            else {
                $controller->params['sortCount'] = 0;
            }
        }
    }

    /**
     * Adiciona os parametros nomeados no array de dados. .
     * Ou seja, faz com que os parametros passados pela paginação
     * (pela url) sejam carregados como dados.
     * @param Controller $controller
     * @access public
     */
    public function startup(&$controller) {
        $action = $controller->params['action'];

        if(!isset($controller->extPaginate) ||
                !isset($controller->extPaginate[$action])) {
            return;
        }

        $args = $controller->passedArgs;
        $filters = $controller->extPaginate[$action];

        foreach($filters as $field => $typeFilter) {

            list($model,$field) = $this->explodeField($field);

            switch($typeFilter) {
                case 'filter':
                case 'filterAll':
                case 'select':
                case 'urlParam':
                    if(isset($args["{$model}.{$field}"])) {
                        $controller->data[$model][$field] = $args["{$model}.{$field}"];
                    }
                    break;
                case 'param':
                // 'param' não é colocado na variável data
                    break;
                default:
                    $this->error('startup', self::NO_TYPE_FILTER_ERROR, $typeFilter);
            }
        }
    }

    /**
     * Como $controller->paginate(...), porém adiciona as condições dos filtros
     * na pesquisa da paginação e na url que será montada.
     * @param mixed $object
     * @param mixed $scope
     * @param array $whitelist
     * @return array Model query results
     * @access public
     */
    public function paginate($object = null, $scope = array(), $whitelist = array()) {
        if(!empty($this->controller->data)) {

            list($url,$conditions) = $this->extractData();

            if(!empty ($conditions)) {
                if(isset($this->controller->paginate['conditions'])) {
                    $conditions = array_merge((array)$conditions, (array)$this->controller->paginate['conditions']);
                }
                $this->controller->paginate['conditions'] = $conditions;
            }

            $orders = $this->sort();

            if(!empty ($orders)) {
                /**
                 * @todo é preciso verificar se isso nao pode gerar erros. sobrescrevendo algum order
                 */
                $this->controller->params['order'] = $orders;
            }
        }

        $result = $this->controller->paginate($object,$scope,$whitelist);

        if(!empty($url)) {
            // adiciona os parametros que serão acrescentados na url da paginação.
            $this->controller->params['search'] = $url;
        }

        //usado para ativar o uso de ajax na paginação
        $this->controller->params['isAjax'] = $this->RequestHandler->isAjax();

        return $result;
    }

    /**
     * Constrói o array de condições (conditions) que será utilizado para fazer a paginação
     *  e array de url que será adicionado nas 'links' da paginação.
     * Isso é feito atraves do array de dados (controller->data) e dos filtros usados (controller->extPaginate)
     * @return array array(url,conditions)
     * @access private
     */
    private function extractData() {
        if(!isset($this->controller->extPaginate)) {
            return;
        }
        $conditions = array();
        $url = array();
        $action = $this->controller->params['action'];
        if(isset($this->controller->extPaginate[$action])) {
            $data = $this->controller->data;
            $filters = $this->controller->extPaginate[$action];

            foreach($filters as $field => $typeField) {

                list($model,$field) = $this->explodeField($field);

                if(isset($data[$model][$field])) {
                    switch($typeField) {
                        case 'filter':
                            $value = $data[ $model ] [ $field ];
                            $conditions["{$model}.{$field} LIKE"] =  $value  . '%';
                            $url["{$model}.{$field}"] = $value;
                            break;
                        case 'filterAll':
                            $value = $data[ $model ] [ $field ];
                            $conditions["{$model}.{$field} LIKE"] =  '%'. $value  . '%';
                            $url["{$model}.{$field}"] = $value;
                            break;
                        case 'select':
                            $value = $data[ $model ] [ $field ];
                            $conditions["{$model}.{$field}"] =  $value;
                            $url["{$model}.{$field}"] = $value;
                            break;
                        case 'urlParam':
                            $value = $data[ $model ] [ $field ];
                            $url["{$model}.{$field}"] = $value;
                            break;
                        case 'param':
                            $value = $data[ $model ] [ $field ];
                            $conditions["{$model}.{$field}"] =  $value;
                            $url[] = $value;
                            break;
                        default:
                            $this->error('extractData',self::NO_TYPE_FILTER_ERROR,$typeField);
                            break;
                    }
                }
            }
        }
        return array($url,$conditions);
    }

    /**
     * Responsavel por retorna o array com as multiplas ordenações.
     *
     * @return array retorna o array para realizar multiplas ordenações ou null caso nao seja multisort.
     */
    protected function sort() {
        if(!$this->multisort) {
            return;
        }

        $ctrl = $this->controller;
        $options = array_merge($ctrl->params, $ctrl->params['url'], $ctrl->passedArgs);

        // para ter certeza que o sort convencional nao vai ser chamado
        unset($ctrl->params['sort']);
        unset($ctrl->params['url']['sort']);
        unset($ctrl->passedArgs['sort']);

        $order = array();

        for($i = 1; $i <= $options['sortCount']; $i++) {
            $direction = null;
            if (isset($options['direction'.$i])) {
                $direction = strtolower($options['direction'.$i]);
            }
            if ($direction != 'asc' && $direction != 'desc') {
                $direction = 'asc';
            }
            $order[$options['sort'.$i]] = $direction;
        }

        return $order;
    }

    /**
     * Dado uma string $field do tipo "model.field" retorna um array com ("model","field")
     * caso nao seja passado o model então "model" será o model padrão do controller.
     * @param string $field
     * @return array array(modelName,FieldName)
     * @access private
     */
    private function explodeField($field) {
        if(strpos($field, '.') !== false) {
            return explode('.', $field);
        } else {
            return array($this->controller->modelClass, $field);
        }
    }

    /**
     * Lançará um erro na tela, exibindo qual método o lançou e uma mensagem.
     * @param string $method nome do método em que o erro foi dectado.
     * @param string $errorMessage mensagem do erro que será exibida
     * @param mixed $argsMessage caso a mensagem ($errorMessage) seja parametrizada poderá estes serão os argumentos
     * @param <type> $errorType tipo do erro: error/warning/notice
     */
    private function error($method, $errorMessage, $argsMessage = null,$errorType = E_USER_WARNING ) {
        trigger_error(sprintf("PaginatorComponent::{$method}() - {$errorMessage}",$argsMessage), $errorType);
    }

}
?>
